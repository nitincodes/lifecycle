package com.airtel.africa.commision;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public final class ApplicationConstants {
	
	public static List<User> userList;
	
	public static List<Admission> caseList;
	
}
