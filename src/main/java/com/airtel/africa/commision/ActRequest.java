package com.airtel.africa.commision;

import lombok.Data;

@Data
public class ActRequest {
	public String actorId;
	public String nextState; 
	public String caseId;
	
}
