package com.airtel.africa.commision;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends JpaRepository<State, Long>{

	List<State> findByContextTypeAndMachineId(ContextType contextType, String machineId);

	List<State> findByContextType(ContextType cpo);
	
	List<State> findByContextTypeAndContext(String contextType, String Context);
}
