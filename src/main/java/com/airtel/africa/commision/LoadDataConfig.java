package com.airtel.africa.commision;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDataConfig {
	
	@Autowired
	private LoadDataServices loadDataServices;
	
	public void buildConstants() {
		ApplicationConstants.userList = loadDataServices.loadUserData();
		ApplicationConstants.caseList = loadDataServices.loadEnrollData();
	} 
}
