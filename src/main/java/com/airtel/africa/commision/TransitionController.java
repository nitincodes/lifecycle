package com.airtel.africa.commision;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.data.jpa.JpaRepositoryStateMachinePersist;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/transition")
public class TransitionController {
	
	@Autowired
	private CaseService caseService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TransitionController.class);
	
	@Autowired
	StateMachineFactory< String, String> stateMachineFactory;
	
	@Autowired
	StateMachinePersist<String,String,Long> stateMachinePersister;
	
	private static final String MACHINE_ID = "CASE-MACHINE";
	
	
	
	@PostMapping(path="/act",consumes=MediaType.APPLICATION_JSON_VALUE)
	public boolean act(@RequestBody ActRequest actRequest) {
		User actor = null;
		Admission case1 = null;
		boolean success = false;
		for(User actor1 : ApplicationConstants.userList) {
			if(actor1.getId().equals(actRequest.actorId)) {
				actor = actor1;
				break;
			}
		}
		
		for(Admission admission : ApplicationConstants.caseList) {
			if(admission.getId().equals(actRequest.caseId)) {
				case1 = admission;
				break;
			}
		}
		//case1 = caseService.search(actRequest.caseId);
		if(null != actor && null != case1 ) {		
			// find state machine from the case id
			// if existing state machine use that
			// if no state machine build a new one and use this for the transition
			// now if transition successful then update the transition in the SM
			StateMachineContext<String,String> stateMachineContext = null;
			
			try {
				stateMachineContext = stateMachinePersister.read(Long.parseLong(case1.getId()));
				StateMachinePersister<String, String, Long> persister = new DefaultStateMachinePersister<String, String, Long>(stateMachinePersister);
				StateMachine<String,String> stateMachine = stateMachineFactory.getStateMachine(actRequest.getCaseId().toString());
				stateMachine.start();
				if(stateMachineContext != null) {
					persister.restore(stateMachine, Long.parseLong(case1.getId()));
				}
				success = stateMachine.sendEvent(MessageBuilder.withPayload(case1.getLastAction()+"-"+actRequest.nextState).build());
				if(success) {
					persister.persist(stateMachine,Long.parseLong(case1.getId()));
					case1.setLastAction(actRequest.nextState);
					case1.setStatus(actRequest.nextState);
				}				
			}
			catch(Exception ex) {
				LOGGER.error("An exception occured while doing this", ex);
			}
		}
		
		return success;
	}
	
	@RequestMapping("/nextStates")
	public List<String> getStates(String actorId) {
		return new ArrayList<>();
	}
	
	@PostMapping("/create/case")
	public Case1 create(@RequestBody Case1 case1) {
		return caseService.save(case1);
		
	}
		
}
