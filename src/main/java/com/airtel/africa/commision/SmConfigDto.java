package com.airtel.africa.commision;

import java.util.List;

public class SmConfigDto {
	private List<StateDto> states;
	private List<TransitionDto> transitions;
	public List<StateDto> getStates() {
		return states;
	}
	public void setStates(List<StateDto> states) {
		this.states = states;
	}
	public List<TransitionDto> getTransitions() {
		return transitions;
	}
	public void setTransitions(List<TransitionDto> transitions) {
		this.transitions = transitions;
	}
}
