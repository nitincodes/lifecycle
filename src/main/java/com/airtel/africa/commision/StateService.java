package com.airtel.africa.commision;

import java.util.List;

import org.springframework.stereotype.Service;


@Service
public interface StateService {

	 State createState(State states);
	
	 List<State> states();
}
