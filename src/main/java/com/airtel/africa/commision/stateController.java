package com.airtel.africa.commision;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping(value = "api/cm/v1/state")
public class stateController {

	@Autowired
	private StateService stateService;

	@Autowired
	private TransitionService transitionService;

	@PostMapping("/States")
	public State states(@RequestBody State states) {
		log.info(" Register state :");
		return stateService.createState(states);
	}

	@GetMapping("/States")
	public List<State> states() {
		log.info(" Register state :");
		return stateService.states();
	}

	@PostMapping("/Transactions-States")
	public TransitionStates transactionStates(@RequestBody TransitionStates transitionData) {
		log.info(" Register trans :");
		return transitionService.createTrans(transitionData);
	}

	@GetMapping("/Transactions-States")
	public List<TransitionStates> transactionStates() {
		log.info(" Register trans :");
		return transitionService.transitionData();
	}
}
