package com.airtel.africa.commision;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.data.jpa.JpaPersistingStateMachineInterceptor;
import org.springframework.statemachine.data.jpa.JpaStateMachineRepository;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;

@EnableJpaRepositories(basePackages = { "com.airtel.africa.commision", "org.springframework.statemachine.data.jpa" })
@EntityScan(basePackages = { "com.airtel.africa.commision", "org.springframework.statemachine.data" })
@EnableStateMachineFactory
public class StateMachineConfig extends StateMachineConfigurerAdapter<String, String> {

//	@Autowired
//	private StateRepository<? extends RepositoryState> stateRepository;
//
//	@Autowired
//	private TransitionRepository<? extends RepositoryTransition> transitionRepository;

	@Autowired
	private JpaStateMachineRepository jpaStateMachineRepository;

	@Bean
	public StateMachineRuntimePersister<String, String, Long> stateMachineRuntimePersister() {
		return new JpaPersistingStateMachineInterceptor<>(jpaStateMachineRepository);
	}

	/*
	 * @Override public void configure(StateMachineModelConfigurer<String, String>
	 * model) throws Exception {
	 * 
	 * model.withModel().factory(modelFactory()); }
	 */

	@Override
	public void configure(StateMachineConfigurationConfigurer<String, String> config) throws Exception {
		config.withPersistence().runtimePersister(stateMachineRuntimePersister());
	}

	/*
	 * @Bean public StateMachineModelFactory<String,String> modelFactory() { return
	 * new CustomSMModelFactory(); }
	 */

	@Override
	public void configure(StateMachineStateConfigurer<String, String> states) throws Exception {
		Set<String> allStates = new HashSet<>();
		List<ActionType1> allActionTypes = Arrays.asList(ActionType1.values());

		for (ActionType1 action : allActionTypes) {
			allStates.add(action.name());
		}
         states.withStates().initial("PENDING").end("ENROLLED").states(allStates);

	}

	@Override
	public void configure(StateMachineTransitionConfigurer<String, String> transitions) throws Exception {
		transitions.withExternal().source("PENDING").target("VERIFY").event("PENDING-VERIFY").and().withExternal()
				.source("PENDING").target("REJECT").event("PENDING-REJECT").and()

				.withExternal().source("VERIFY").target("ACCEPT").event("VERIFY-ACCEPT").and().withLocal()
				.source("ACCEPT").target("PAYMENT_PENDING").event("ACCEPT-PAYMENT_PENDING").and()

				.withExternal().source("PAYMENT_PENDING").target("PAYMENT_APPROVE").event("PAYMENT_PENDING-PAYMENT_APPROVE").and().withExternal()
				.source("PAYMENT_APPROVE").target("ENROLLED").event("PAYMENT_APPROVE-ENROLLED");

	}

}
