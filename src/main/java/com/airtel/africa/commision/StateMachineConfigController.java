package com.airtel.africa.commision;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.data.StateRepository;
import org.springframework.statemachine.data.TransitionRepository;
import org.springframework.statemachine.data.jpa.JpaRepositoryState;
import org.springframework.statemachine.data.jpa.JpaRepositoryTransition;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sm-config")
public class StateMachineConfigController {
	
	@Autowired
	StateRepository<JpaRepositoryState> stateRepository;

	@Autowired
	TransitionRepository<JpaRepositoryTransition> transitionRepository;
	
	private static final String MACHINE_ID = "CASE-MACHINE";

	@RequestMapping("/create")
	public Map createConfig(@RequestBody SmConfigDto smConfigDto) {
		Map responseMap = new HashMap<>();
		List<JpaRepositoryState> repositoryStates = new ArrayList<>();
		Map<String,JpaRepositoryState> repositoryStateMap = new HashMap<>();
				
		for(StateDto stateDto : smConfigDto.getStates()) {
			String stateName = stateDto.getName();
			boolean isInitial = stateDto.isInitial();
			JpaRepositoryState jpaRepositoryState = new JpaRepositoryState(MACHINE_ID,stateName, isInitial);
			//repositoryStateMap.put(stateName, jpaRepository);
			repositoryStates.add(jpaRepositoryState);
		}
		
		List<JpaRepositoryState> savedStates = null;
		List<JpaRepositoryTransition> savedTransitions = null;
		if(repositoryStates.size() > 0) {
			stateRepository.saveAll(repositoryStates);
			for(JpaRepositoryState savedState : repositoryStates) {
				repositoryStateMap.put(savedState.getState(), savedState);
			}
			List<JpaRepositoryTransition> repositoryTransitions = new ArrayList<>();
			for(TransitionDto transitionDto : smConfigDto.getTransitions()) {
				repositoryTransitions.add(new JpaRepositoryTransition(MACHINE_ID,repositoryStateMap.get(transitionDto.getSource()),
						repositoryStateMap.get(transitionDto.getTarget()),
						transitionDto.getEvent()));
			}
			transitionRepository.saveAll(repositoryTransitions);
			responseMap.put("states",repositoryStates);
			responseMap.put("transitions",repositoryTransitions);
			//savedStates = new ArrayList<>() 
		}
		else {
			responseMap.put("error", "Cannot save the machine");
		}		
		return responseMap;
	}
	
}

