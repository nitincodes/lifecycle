package com.airtel.africa.commision;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransitionsServiceimpl implements TransitionService{

	@Autowired
	private TransitionRepository transitionRepository;
	
	@Override
	public TransitionStates createTrans(TransitionStates transitionData) {
		return transitionRepository.save(transitionData);
	}

	@Override
	public List<TransitionStates> transitionData() {
		return transitionRepository.findAll();
	}

}
