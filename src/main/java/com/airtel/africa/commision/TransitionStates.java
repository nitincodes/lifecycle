package com.airtel.africa.commision;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;


@Entity
@Table(name = "transitionStates")
@Data
public class TransitionStates {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;
	
	@Column(name = "source")
	private String source;
	
	@Column(name = "event")
	private String event;
	
	@Column(name = "target")
	private String target;
	
	@Column(name = "context")
	@Enumerated(EnumType.STRING)
	private Context context;
	
	@Column(name = "contextType", nullable = false)
	@Enumerated(EnumType.STRING)
	private ContextType contextType;
	
	@Column(name = "security")
	private String security;
	
	@Column(name = "machineId")
	private String machineId;
	  
 
}
