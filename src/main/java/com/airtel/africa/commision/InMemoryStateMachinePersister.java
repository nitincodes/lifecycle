package com.airtel.africa.commision;

import java.util.HashMap;

import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.stereotype.Component;

@Component
public class InMemoryStateMachinePersister implements StateMachinePersist<String, String, String> {
	
	private final HashMap<String, StateMachineContext<String, String>> contexts = new HashMap<>(); 
	
	@Override
	public void write(StateMachineContext<String, String> context, String contextObj) throws Exception {
		// TODO Auto-generated method stub
		contexts.put(contextObj, context);
	}

	@Override
	public StateMachineContext<String, String> read(String contextObj) throws Exception {
		// TODO Auto-generated method stub
		return contexts.get(contextObj);
		
	}	
}


