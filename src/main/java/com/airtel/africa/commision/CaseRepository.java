package com.airtel.africa.commision;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseRepository extends CrudRepository<Case1, Long> {

	Optional<Case1> findById(Long id);
	
	Optional<Case1> findByCaseId(String caseId);
}
