package com.airtel.africa.commision;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CaseService {
	
	@Autowired
	CaseRepository caseRepository;
	
	public Case1 save(Case1 case1) {
		return caseRepository.save(case1);
	}
	
	public Case1 search(Long caseId) {
		return caseRepository.findById(caseId).get();
	}
	
}
