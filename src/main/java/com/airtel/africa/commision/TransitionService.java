package com.airtel.africa.commision;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface TransitionService  {

	TransitionStates createTrans(TransitionStates transitionData);
	
	 List<TransitionStates> transitionData();
}
