package com.airtel.africa.commision;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.config.builders.StateMachineConfigurationBuilder;
import org.springframework.statemachine.config.model.ConfigurationData;
import org.springframework.statemachine.config.model.DefaultStateMachineModel;
import org.springframework.statemachine.config.model.StateData;
import org.springframework.statemachine.config.model.StateMachineModel;
import org.springframework.statemachine.config.model.StateMachineModelFactory;
import org.springframework.statemachine.config.model.StatesData;
import org.springframework.statemachine.config.model.TransitionData;
import org.springframework.statemachine.config.model.TransitionsData;
import org.springframework.stereotype.Component;

import com.esotericsoftware.minlog.Log;

//@Component
public class CustomSMModelFactory implements StateMachineModelFactory<String, String> {

	/**
	 * Add two entities for states and transitions Autowire the repos here and build
	 * the stateData with the help of repos here. Add the data in repos with the
	 * help of those repositories here
	 */

	@Autowired
	private TransitionRepository transitionRepository;

	@Autowired
	private StateRepository stateRepository;

	@Override
	public StateMachineModel<String, String> build() {
		ConfigurationData<String, String> configurationData = new ConfigurationData<String, String>();
		Collection<StateData<String, String>> stateData = new ArrayList<>();
		List<State> states = stateRepository.findByContextType(ContextType.CPO);
		for (State s : states) {
			stateData.add(new StateData<String, String>(s.getState(), true));
			stateData.add(new StateData<String, String>(null, null, s.getState(), true));
		}
		StatesData<String, String> statesData = new StatesData<>(stateData);
		Collection<TransitionData<String, String>> transitionData = new ArrayList<>();
		List<TransitionStates> trans = transitionRepository.findByContextType(ContextType.CPO);
		for (TransitionStates transi : trans) {
			transitionData.add(new TransitionData<String, String>(transi.getSource().toString(),
					transi.getTarget().toString(), transi.getEvent()));
		}
		TransitionsData<String, String> transitionsData = new TransitionsData<>(transitionData);
		StateMachineModel<String, String> stateMachineModel = new DefaultStateMachineModel(configurationData,
				statesData, transitionsData);
		return stateMachineModel;
	}

	public StateMachineModel<String, String> build(String machineId) {
		StateMachineConfigurationBuilder<String, String> stateMachineConfigurationBuilder = new StateMachineConfigurationBuilder<String, String>();
		stateMachineConfigurationBuilder.setMachineId(machineId);
		try {
			ConfigurationData<String, String> configurationData = stateMachineConfigurationBuilder.build();
			Collection<StateData<String, String>> stateData = new ArrayList<>();
			List<State> states = stateRepository.findByContextType(ContextType.CPO);
			for (State s : states) {
				StateData<String,String> currentState = new StateData<>(s.getState());
				currentState.setEnd(s.isEndState());
				currentState.setInitial(s.isInitialState());
				stateData.add(currentState);
			}
			StatesData<String, String> statesData = new StatesData<>(stateData);
			Collection<TransitionData<String, String>> transitionData = new ArrayList<>();
			List<TransitionStates> trans = transitionRepository.findByContextType(ContextType.CPO);
			for (TransitionStates transi : trans) {
				transitionData.add(new TransitionData<String, String>(transi.getSource(),
						transi.getTarget(), transi.getEvent()));
			}
			TransitionsData<String, String> transitionsData = new TransitionsData<>(transitionData);
			StateMachineModel<String, String> stateMachineModel = new DefaultStateMachineModel<>(configurationData,
					statesData, transitionsData);
			return stateMachineModel;
		} catch (Exception e) {
			Log.error(e.getLocalizedMessage());
		}
		return null;
	}

}
