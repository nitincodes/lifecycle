package com.airtel.africa.commision;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

@Entity
@Table(name = "stateInfo")
@Data
public class State {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;

	@Column(name = "context")
	@Enumerated(EnumType.STRING)
	private Context context;

	@Column(name = "currentState")
	private String state;

	@Column(name = "initialState")
	private boolean initialState;

	@Column(name = "endState")
	private boolean endState;

	@Column(name = "contextType", nullable = false)
	@Enumerated(EnumType.STRING)
	private ContextType contextType;

	@Column(name = "region")
	private String region;

	@Column(name = "machineId")
	private String machineId;
	

}
