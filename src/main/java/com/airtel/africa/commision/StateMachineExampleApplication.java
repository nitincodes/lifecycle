package com.airtel.africa.commision;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationContextInitializedEvent;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;

@SpringBootApplication
public class StateMachineExampleApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(StateMachineExampleApplication.class);
	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(StateMachineExampleApplication.class);
		addInitHooks(springApplication);
		springApplication.run(args);
	}
	
	public static void addInitHooks(SpringApplication application) {
		
		List<ApplicationContextEvent> events = new ArrayList<>();
		
		ApplicationListener<ApplicationReadyEvent> applicationListener = new ApplicationListener<ApplicationReadyEvent>() {
			
			@Override
			public void onApplicationEvent(ApplicationReadyEvent event) {
				LOGGER.debug("On applicaiton events");
				LOGGER.debug(event.toString());
				LoadDataConfig loadConfigData = (LoadDataConfig)event.getApplicationContext().getBean("loadDataConfig");
				loadConfigData.buildConstants();
				
			}
		};
		application.addListeners(applicationListener);
	}
}
