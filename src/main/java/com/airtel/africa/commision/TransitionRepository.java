package com.airtel.africa.commision;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransitionRepository extends JpaRepository<TransitionStates, Long>{ 

List<TransitionStates> findByContextTypeAndMachineId(ContextType contextType,Long machine);

List<TransitionStates> findByContextType(ContextType cpo);


}
