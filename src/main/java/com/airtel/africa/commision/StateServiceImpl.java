package com.airtel.africa.commision;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StateServiceImpl implements StateService {

	@Autowired
	private StateRepository stateRepository;

	@Override
	public State createState(State states) {
		return stateRepository.save(states);
	}

	@Override
	public List<State> states() {
		return stateRepository.findAll();
	}

}
