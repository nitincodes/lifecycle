package com.airtel.africa.commision;

public enum Department {
	HUMAN_RESOURCE, ENGINEERING, BUSINESS_ADMINISTRATION
}
