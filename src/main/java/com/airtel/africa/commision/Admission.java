package com.airtel.africa.commision;

import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Table(name = "EnrollUser")
@AllArgsConstructor
@NoArgsConstructor
public class Admission {
	private String id;
	private String departmentType;
	private String contextType;
	private String status;
	private String enrollmentId;
	private String userName;
	private String higherEductaion;
	private String score;
	private String documentNo;
	private String lastAction;
}
