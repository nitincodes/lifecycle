package com.airtel.africa.commision;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class LoadDataServices {
	
	/*
	 * public List<Actor1> loadUserData() { Actor1 user1 = new Actor1(); user1.id =
	 * "DE1"; user1.actorType = getDERole();
	 * 
	 * Actor1 user2 = new Actor1(); user2.id = "SU1"; user2.actorType =
	 * getSuperviorRole();
	 * 
	 * List<Actor1> actorList = new ArrayList<>(); actorList.add(user1);
	 * actorList.add(user2); return actorList; }
	 * 
	 * public List<Case1> loadCaseData() { Case1 case1 = new Case1();
	 * case1.setCaseId("1"); case1.setBusinessLine(BusinessLine1.CPO.name());
	 * case1.setLastAction("PENDING");
	 * 
	 * Case1 case2 = new Case1(); case2.setCaseId("2");
	 * case2.setBusinessLine(BusinessLine1.KYC.name());
	 * case2.setLastAction("PENDING");
	 * 
	 * List<Case1> caseList = new ArrayList<>(); caseList.add(case1);
	 * caseList.add(case2); return caseList;
	 * 
	 * }
	 */
	

	public List<User> loadUserData() {
		User user1 = new User();
		user1.setDepartment(Department.BUSINESS_ADMINISTRATION);
		user1.setUserRole("Accounts Verify");
		user1.setUserName("sub Admin");
		user1.setId("1");

	    User user2 = new User();
		user2.setDepartment(Department.BUSINESS_ADMINISTRATION);
		user2.setUserRole("Accounts Manager");
		user2.setUserName("Admin");
		user2.setId("2");
		
		User user3= new User();
		user3.setDepartment(Department.BUSINESS_ADMINISTRATION);
		user3.setUserRole("Admission Manager");
		user3.setUserName("Super Admin");
		user2.setId("3");


		List<User> UserList = new ArrayList<>();
		UserList.add(user1);
		UserList.add(user2);
		UserList.add(user3);
		return UserList;
	}

	public List<Admission> loadEnrollData() {
		Admission admission = new Admission();
		admission.setId("1");
		admission.setDepartmentType(Department.BUSINESS_ADMINISTRATION.name());
		admission.setLastAction("PENDING");
		admission.setDocumentNo("2");
		admission.setContextType(Context.FORM.name());
		admission.setHigherEductaion("12th");
		admission.setScore("60%");
		admission.setUserName("Lovely");
		admission.setStatus("PENDING");
		
		/*
		 * Admission admission2 = new Admission(); admission2.setId("2");
		 * admission2.setDepartmentType(Department.BUSINESS_ADMINISTRATION.name());
		 * admission2.setLastAction("PENDING"); admission2.setDocumentNo("2");
		 * admission2.setContextType(Context.FORM.name());
		 * admission2.setHigherEductaion("12th"); admission2.setScore("65%");
		 * admission2.setUserName("Rishabh"); admission2.setStatus("PENDING");
		 */
		
		List<Admission> list = new ArrayList<>();
		list.add(admission);
		//list.add(admission2);
		return list;

	}
	/*
	 * private Set<String> getDERole() { List<String> roles = new ArrayList<>();
	 * roles.add("DE"); return new HashSet<>(roles); }
	 * 
	 * private Set<String> getSuperviorRole() { List<String> roles = new
	 * ArrayList<>(); roles.add("SU"); return new HashSet<>(roles); }
	 */
	
}
